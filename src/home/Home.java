package home;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Home {

	public static int all(int tips) {

		System.out.println("ゲームを開始します。");
		System.out.println("現在のチップは" + tips + "点です。");

		List<Integer> deck = new ArrayList<>(52);
		shuffleDeck(deck);

		List<Integer> player = new ArrayList<>(10);
		List<Integer> dealer = new ArrayList<>(10);

		player.add(deck.get(0));
		dealer.add(deck.get(1));
		player.add(deck.get(2));
		dealer.add(deck.get(3));

		int playerHands = 2;
		int dealerHands = 2;
		int deckCount = 4;

		System.out.println("掛け金を決めてください。(数字を入力)");

		Scanner scan = new Scanner(System.in);
		int bet = 0;

		while(true) {
			String obj = scan.next();
			if (obj.matches("^[0-9]+$")) {
				bet = Integer.parseInt(obj);
				if(bet > tips) {
					System.out.println("掛け金は所持チップ数以内で設定してください。");
				}else {
					System.out.println("掛け金は" + bet + "点です。");
					break;
				}
			}else {
				System.out.println("あなたの入力は" + obj + " です。数字を入力してください。");
			}
		}

		System.out.println("あなたの1枚目のカードは" + toDescription(player.get(0)));

		System.out.println("ディーラーの1枚目のカードは" + toDescription(dealer.get(0)));

		System.out.println("あなたの2枚目のカードは" + toDescription(player.get(1)));

		System.out.println("ディーラーの2枚目のカードは秘密です。");

		int playerPoint = sumPoint(player);
		int dealerPoint = sumPoint(dealer);
		int action = 0;

		System.out.println("あなたの現在の合計ポイントは" + playerPoint + "です。" );
		if (playerPoint == 21) {
			System.out.println("Black Jack!!!");
			System.out.println("");
			System.out.println("ディーラーのターンに移ります。");
			System.out.println("ディーラーのの2枚目のカードは" + toDescription(dealer.get(1)) + "でした。");
			System.out.println("ディーラーの合計ポイントは"+ dealerPoint + "でした。");
			if (dealerPoint == 21) {
				System.out.println("ディーラーの勝ちです。どんまい、、、！");
				return tips - bet;
			}else {
				System.out.println("あなたの勝ちです。BJボーナスで掛け金が2.5倍になりました!");
				int bonus = bet / 2;
				return tips + bet + bonus;
			}
		}else {
			while(true) {
				System.out.println("サレンダー(ベット額の半額だけ払い、勝負を降りる)しますか？Yes:y or No:n");
				String str = scan.next();
				if("n".equals(str)) {
					break;
				}else if("y".equals(str)) {
					int surender = bet / 2;
					System.out.println("掛け金の半分" + surender + "で勝負を降りました。");
					return tips - surender;
				}else {
					System.out.println("あなたの入力は" + str + " です。y か n を入力してください。");
				}
			}
			while(true) {
				System.out.println("追加でカードを引きますか？Yes:y or No:n");
				String add = scan.next();
				if("n".equals(add)) {
					break;
				} else if("y".equals(add)) {
					if(action == 0) {
						System.out.println("ダブルダウン(次に1枚だけ引いて終了する代わりに、bet額を2倍にする)しますか？Yes:y or No:n");
						add = scan.next();
						if("n".equals(add)) {
							player.add(deck.get(deckCount));
							deckCount++;
							playerHands++;
							System.out.println("あなたの" + playerHands + "枚目のカードは" + toDescription(player.get(playerHands - 1)));
							playerPoint = sumPoint(player);
							System.out.println("現在の合計ポイントは" + playerPoint);
							action++;
							if(isBusted(playerPoint)) {
								System.out.println("残念、、、バーストしてしまいました。あなたの負けです。");
								return tips - bet;
							}
						}else if("y".equals(add)) {
							if(bet * 2 > tips - bet) {
								System.out.println("残りチップ数が足りません。");
							}else {
								bet = bet * 2;
								System.out.println("掛け金は" + bet + "になりました。");
								player.add(deck.get(deckCount));
								deckCount++;
								playerHands++;
								System.out.println("あなたの" + playerHands + "枚目のカードは" + toDescription(player.get(playerHands - 1)));
								playerPoint = sumPoint(player);
								System.out.println("現在の合計ポイントは" + playerPoint);
								if(isBusted(playerPoint)) {
									System.out.println("残念、、、バーストしてしまいました。あなたの負けです。");
									return tips - bet;
								}
								break;
							}
						} else {
							System.out.println("あなたの入力は" + add + " です。y か n を入力してください。");
						}
					}else {
						player.add(deck.get(deckCount));
						deckCount++;
						playerHands++;
						System.out.println("あなたの" + playerHands + "枚目のカードは" + toDescription(player.get(playerHands - 1)));
						playerPoint = sumPoint(player);
						System.out.println("現在の合計ポイントは" + playerPoint);
						if(isBusted(playerPoint)) {
							System.out.println("残念、、、バーストしてしまいました。あなたの負けです。");
							return tips - bet;
						}
					}

				} else {
					System.out.println("あなたの入力は" + add + " です。y か n を入力してください。");
				}
			}
		}

		System.out.println("ディーラーのターンに移ります。");
		System.out.println("ディーラーのの2枚目のカードは" + toDescription(dealer.get(1)) + "でした。");
		while(true) {
			if(dealerPoint >= 17) {
				break;
			} else {
				dealer.add(deck.get(deckCount));
				deckCount++;
				dealerHands++;
				System.out.println("ディーラーの" + dealerHands + "枚目のカードは" + toDescription(dealer.get(dealerHands - 1)));
				dealerPoint = sumPoint(dealer);
				if(isBusted(dealerPoint)) {
					System.out.println("ディーラーがバーストしました。あなたの勝ちです！");
					return tips + bet;
				}
			}
		}

		System.out.println("あなたの合計ポイントは" + playerPoint + "。");
		System.out.println("ディーラーの合計ポイントは"+ dealerPoint + "。");

		if(playerPoint == dealerPoint) {
			System.out.println("引き分けです。");
			return tips;
		} else if(playerPoint > dealerPoint) {
			System.out.println("勝ちました！");
			return tips + bet;
		} else {
			System.out.println("負けました・・・");
			return tips - bet;
		}

	}

	private static void shuffleDeck(List<Integer> deck){
		for(int i = 1; i <= 52; i++){
			deck.add(i);
		}

		Collections.shuffle(deck);
	}

	private static String toDescription(int cardNumber) {
		String rank = toRank(toNumber(cardNumber));
		String suit = toSuit(cardNumber);

		return suit + "の" + rank;
	}

	private static String toSuit(int cardNumber) {
		switch((cardNumber - 1)/13) {
		case 0:
			return "♣";
		case 1:
			return "♦";
		case 2:
			return "♥";
		case 3:
			return "♠";
		default:
			return "例外です";
		}
	}

	private static int toNumber(int cardNumber) {
		int number = cardNumber % 13;
		if(number == 0) {
			number = 13;
		}

		return number;
	}

	private static String toRank(int number) {
		switch(number) {
		case 1:
			return "A";
		case 11:
			return "J";
		case 12:
			return "Q";
		case 13:
			return "K";
		default:
			String str = String.valueOf(number);
			return str;
		}
	}

	private static int toPoint(int num) {
		if(num ==11||num == 12||num == 13) {
			num = 10;
		}else if (num == 1) {
			num = 11;
		}
		return num;
	}

	private static int sumPoint(List<Integer> list) {
		int sum = 0;
		int aceCount = 0;

		for(int i =0;i < list.size();i++) {
			sum = sum + toPoint(toNumber(list.get(i)));
			if(toNumber(list.get(i)) == 1) {
				aceCount += 1;
			}
		}
		if(sum > 21 && aceCount > 0) {
				for (int j = aceCount;(j > 0 && sum > 21);j--) {
					sum -= 10;
				}
		}
		return sum;
	}

	private static boolean isBusted(int point) {
		if (point <= 21) {
			return false;
		} else {
			return true;
		}
	}
}
